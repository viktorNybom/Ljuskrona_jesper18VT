
#include <Adafruit_NeoPixel.h>
#include <EEPROM.h>

#ifdef __AVR__
  #include <avr/power.h>
#endif

#define NUMPIXELS      100
#define PIN            6
Adafruit_NeoPixel pixels = Adafruit_NeoPixel(NUMPIXELS, PIN, NEO_GRB + NEO_KHZ800);

int state = 0;

byte redRef[NUMPIXELS];
byte greenRef[NUMPIXELS];
byte blueRef[NUMPIXELS];
byte red[NUMPIXELS];
byte green[NUMPIXELS];
byte blue[NUMPIXELS];
byte ledToDim[NUMPIXELS];
volatile uint8_t modeNow;
unsigned long lastModeTime;
volatile uint8_t lastMode;

volatile int chModeDetect;
int nbrOfModes=16;
uint8_t modeTemp[10];
int rmode=0;
unsigned int counter=0;


volatile unsigned int intervall = 400;
unsigned long lastTime = 0;
unsigned int PWMintervall = 1000;
unsigned long PWMlastTime = 0;
volatile unsigned int dimIntervall = 20;
unsigned long lastDimT = 0;

void setup() {
  // put your setup code here, to run once:
  state = 0;
  randomSeed(analogRead(0));
  // This is for Trinket 5V 16MHz, you can remove these three lines if you are not using a Trinket
#if defined (__AVR_ATtiny85__)
  if (F_CPU == 16000000) clock_prescale_set(clock_div_1);
#endif
  // End of trinket special code
   
  loadConfigSettings();
  delay(10);
  if(modeNow > 0){
    modeNow=0;
  }else{
    modeNow=1;
  }
  lastMode = modeNow;
  saveConfigSettings();
  pixels.begin(); // This initializes the NeoPixel library.
  lastModeTime = millis();
  
}

void loop() {
   if(lastMode == modeNow){
    if(millis() - lastModeTime > 5000){
      delay(10);
      state=0;
      if(modeNow > 0){
        modeNow=0;
      }else{
        modeNow=1;
      }
      saveConfigSettings();
      lastMode = modeNow;
      if(modeNow > 0){
        modeNow=0;
      }else{
        modeNow=1;
      }
    }
  }
  mode(modeNow);
  ledDim();
}

void mode(int val){

  switch(val){
    
  case 0:
    if(state!=2){
       setAll(255,70,0);
       intervall=100;
       state=2;
    }
    for(int i=0; i < NUMPIXELS; i++){
      if(ledToDim[i]==0 && green[i]==140){
          setDim(i,255,70,0,2+random(5));          
      }
    }
  
    if(millis() - lastTime > (intervall+random(10))<<(1+random(3))){
        int r = random(NUMPIXELS);
        if(ledToDim[r]==0 && green[r]==70){
          setDim(r,255,140,30,1+random(10));
        }
        lastTime=millis();
    }
   break;
   case 1:
   if(state!=2){
       setAll(0,255,0);
       intervall=100;
       state=2;
    }
    for(int i=0; i < NUMPIXELS; i++){
      if(ledToDim[i]==0 && green[i]==0){
          setDim(i,0,255,0,2+random(5));          
      }
    }
  
    if(millis() - lastTime > (intervall+random(10))<<(1+random(3))){
        int r = random(NUMPIXELS);
        if(ledToDim[r]==0 && green[r]==255){
          setDim(r,255,0,255,1+random(10));
        }
        lastTime=millis();
    }
    break;
  }
   
}
void ledDim(){
  if(millis() - lastDimT > dimIntervall){
    counter++;
    int tr=0;
    int tg=0;
    int tb=0;
    int tw=0;
    int ch=0;
    for(int i =0; i < NUMPIXELS; i++){
      if(ledToDim[i]!=0){
        ch=1;
        if(ledToDim[i]<128){
          if(red[i]==redRef[i] && blue[i]==blueRef[i] && green[i]==greenRef[i]){
            ledToDim[i]=0;
          }
          tr=red[i];
          tg=green[i];
          tb=blue[i];
          if(redRef[i]>tr){
            tr+=ledToDim[i];
            if(tr>redRef[i]){
              tr=redRef[i];
            }
          }else if(tr>redRef[i]){
            tr-=ledToDim[i];
            if(tr<redRef[i]){
              tr=redRef[i];
            }
          }
          if(greenRef[i]>tg){
            tg+=ledToDim[i];
            if(tg>greenRef[i]){
              tg=greenRef[i];
            }
          }else if(tg>greenRef[i]){
            tg-=ledToDim[i];
            if(tg<greenRef[i]){
              tg=greenRef[i];
            }
          }
          if(blueRef[i]>tb){
            tb+=ledToDim[i];
            if(tb>blueRef[i]){
              tb=blueRef[i];
            }
          }else if(tb>blueRef[i]){
            tb-=ledToDim[i];
            if(tb<blueRef[i]){
              tb=blueRef[i];
            }
          }
          red[i]=tr;
          green[i]=tg;
          blue[i]=tb;
          pixels.setPixelColor(i, pixels.Color(red[i],green[i],blue[i])); 
        }
      }
     
    }
    if(ch){
      pixels.show();
      
    }
    lastDimT=millis();
  }
}
  
void setRandomDim(int color,int stepspeed){
  int led=random(NUMPIXELS);
    switch(random(6)){
    case 0:
    setDim(led,color,0,0,stepspeed);
    break;
    case 1:
    setDim(led,0,color,0,stepspeed);
    break;
    case 2:
    setDim(led,0,0,color,stepspeed);
    break;
    case 3:
    setDim(led,color,color,0,stepspeed);
    break;
    case 4:
    setDim(led,0,color,color,stepspeed);
    break;
    case 5:
    setDim(led,color,0,color,stepspeed);
    break;
  }
}
void setRandomDim(int r, int g, int b){
  int led=random(NUMPIXELS);
  setDim(led,r,g,b,10);
}

void setDimAll(int r, int g, int b, int stepspeed){
  for(int i=0;i<NUMPIXELS;i++){
    setDim(i,r,g,b,stepspeed);
  }
}
void ledOverflowFix(int led, int r, int g, int b){
  while(led<0){
    led+=NUMPIXELS;
  }
  while(led>NUMPIXELS){
    led-=NUMPIXELS;
  }
  pixels.setPixelColor(led, pixels.Color(r,g,b));
}
  
void setDim(int led, int r, int g, int b, int stepspeed){
  ledToDim[led]=stepspeed;
  redRef[led]=r;
  blueRef[led]=b;
  greenRef[led]=g;
}
void flow(int r, int g, int b,int flowSpeed){
  if(flowSpeed>0){
    if(millis()-lastTime>flowSpeed){
     if(state<NUMPIXELS){
      setAll(0,0,0);
      pixels.setPixelColor(state, pixels.Color(r,g,b));
      pixels.show();
      state++;
     }else{
      state=0;
     }
     lastTime=millis();
    }
  }else if(flowSpeed<0){
     if(millis()-lastTime>flowSpeed){
     if(0<state){
      setAll(0,0,0);
      pixels.setPixelColor(state, pixels.Color(r,g,b));
      pixels.show();
      state--;
     }else{
      state=NUMPIXELS;
     }
     lastTime=millis();
    }
  }
}
void setRandom(int color){
  int led=random(NUMPIXELS);
  switch(random(6)){
    case 0:
    pixels.setPixelColor(led, pixels.Color(color,0,0));
    break;
    case 1:
    pixels.setPixelColor(led, pixels.Color(0,color,0));
    break;
    case 2:
    pixels.setPixelColor(led, pixels.Color(0,0,color));
    break;
    case 3:
    pixels.setPixelColor(led, pixels.Color(color,color,0));
    break;
    case 4:
    pixels.setPixelColor(led, pixels.Color(0,color,color));
    break;
    case 5:
    pixels.setPixelColor(led, pixels.Color(color,0,color));
    break;
    
  }
  pixels.show();
}
void setRandom(int red,int green, int blue){
  int led=random(NUMPIXELS);
  
  pixels.setPixelColor(led, pixels.Color(red,green,blue));

  pixels.show();
}
void strobe(int red,int blue, int green, int ton, int toff){
      if(millis() - lastTime > toff){
        setAll(red,blue,green);
        delay(ton);    
        setAll(0,0,0);
       lastTime=millis();
      }     
}void setLed(int i, int r, int g, int b){
    red[i]=r;
    green[i]=g;
    blue[i]=b;
    redRef[i]=r;
    greenRef[i]=g;
    blueRef[i]=b;
    ledToDim[i]=0;
    pixels.setPixelColor(i, pixels.Color(r,g,b));
}
void setAll(int r, int g, int b){
  for(int i=0;i<NUMPIXELS;i++){
    // pixels.Color takes RGB values, from 0,0,0 up to 255,255,255
    red[i]=r;
    green[i]=g;
    blue[i]=b;
    redRef[i]=r;
    greenRef[i]=g;
    blueRef[i]=b;
    ledToDim[i]=0;
    pixels.setPixelColor(i, pixels.Color(r,g,b));
  }
  pixels.show();
}

int saveConfigSettings(){
    //rewrite last_address every save.
    
    EEPROM.write(0,modeNow);
    return 1;
}
int loadConfigSettings(){
    modeNow = EEPROM.read(0);
    return 1;    
}
//Methods  to write and read 16bit words to eeprom
void EEPROMWrite16(int address, uint16_t value){
  byte two = (value & 0xFF);
  byte one = ((value >> 8) & 0xFF);

  EEPROM.write(address, two);
  EEPROM.write(address + 1, one);
  
}
uint16_t EEPROMRead16(int address){
  long two = EEPROM.read(address);
  long one = EEPROM.read(address+1);
  return ((two << 0) & 0xFF) + ((one << 8) & 0xFFFF);
}

